# CSS Reset
## Meyer Reset + box-sizing hack

### Français (English below)
Les navigateurs ont leur propre feuille de style par défaut qui ne sont pas
forcément harmonisées et qui s'appliquent par défaut sur toutes les balises.

Par exemple, une balise H2 est rendue avec font-size:16px; et 
margin-bottom: 12px; sur un navigateur alors que sur un autre elle peut avoir 
font-size: 18px; et margin-bottom: 14px;

Donc, si vous n'écrivez pas de règles pour vos balises H2, elles ne seront pas 
affichées de façon identique sur tous les navigateurs.

Le CSS Reset permet d'harmoniser le rendu de chaque balises sur tous les 
navigateurs.

Il existe plusieurs CSS Reset.

Certains mettent tout à zéro (aucun style pour toutes les balises). 
C'est le Cas du CSS Reset d'Eric Meyer :
http://meyerweb.com/eric/thoughts/2007/05/01/reset-reloaded/

D'autres mettent des valeurs par défaut, comme Normalize.css : 
https://necolas.github.io/normalize.css/

Pour ma part, j'utilise le Reset d'Eric Meyer auquel j'ai ajouté le 
"box-sizing hack" : https://css-tricks.com/box-sizing/

Vous pouvez télécharger ma version ici :
https://gitlab.com/lozit/css_reset_box_sizing_hack/raw/master/reset.css?inline=false

------------
### English
Browsers have their own default style sheet that are not
necessarily harmonized and which apply by default to all tags.

For example, an H2 tag is rendered with font-size:16px; and 
margin-bottom: 12px; on one browser while on another it may have 
font-size: 18px; and margin-bottom: 14px;

So, if you don't write rules for your H2 tags, they won't be 
displayed in the same way on all browsers.

The CSS Reset allows to harmonize the rendering of each tag on all 
browsers.

There are several CSS Reset.

Some set everything to zero (no style for all tags). 
This is the case of Eric Meyer's CSS Reset:
http://meyerweb.com/eric/thoughts/2007/05/01/reset-reloaded/

Others set default values, such as Normalize.css: 
https://necolas.github.io/normalize.css/

For my part, I use Eric Meyer's Reset to which I added the 
"box-sizing hack": https://css-tricks.com/box-sizing/

You can download my version here:
https://gitlab.com/lozit/css_reset_box_sizing_hack/raw/master/reset.css?inline=false

